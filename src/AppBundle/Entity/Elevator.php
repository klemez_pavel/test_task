<?php 

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ElevatorRepository")
 * @ORM\Table(name="elevator")
 */

class Elevator 
{

    const UP = 1;
    const DOWN = 2;
    const TOP_FLOOR = 25;
    const GROUND_FLOOR = 1;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $minFloor;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxFloor;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentFloor;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMinFloor()
    {
        return $this->minFloor;
    }

    /**
     * @param mixed $minFloor
     */
    public function setMinFloor($minFloor)
    {
        $this->minFloor = $minFloor;
    }

    /**
     * @return mixed
     */
    public function getMaxFloor()
    {
        return $this->maxFloor;
    }

    /**
     * @param mixed $maxFloor
     */
    public function setMaxFloor($maxFloor)
    {
        $this->maxFloor = $maxFloor;
    }

    /**
     * @return mixed
     */
    public function getCurrentFloor()
    {
        return $this->currentFloor;
    }

    /**
     * @param mixed $currentFloor
     */
    public function setCurrentFloor($currentFloor)
    {
        $this->currentFloor = $currentFloor;
    }

    public function up() 
    {

    }

    public function down()
    {

    }

    public function toFloor()
    {
        
    }
}