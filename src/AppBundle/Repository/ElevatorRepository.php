<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Elevator;
use Doctrine\ORM\EntityRepository;

class ElevatorRepository extends EntityRepository
{
    public function findNearestElevator($floorNumber, $departureFloor) : Elevator
    {
        $elevators = $this->getEntityManager()
            ->getRepository(Elevator::class)
            ->findAll();
        $id = null;
        $diff =  Elevator::TOP_FLOOR;
        $currentDiff = null;
        foreach ($elevators as $elevator) {
            if($elevator->getMaxFloor() < $floorNumber) {
                continue;
            }
            if($elevator->getMaxFloor() < $departureFloor) {
                continue;
            }
            $currentDiff = abs($departureFloor-$elevator->getCurrentFloor());
            if($diff == $currentDiff) {
                continue;
            }
            if($diff > $currentDiff) {
                $nearestElevator = $elevator;
                $diff = $currentDiff;
            }
        }
        return $nearestElevator;
    }
}