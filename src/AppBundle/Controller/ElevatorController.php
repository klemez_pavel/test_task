<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Elevator;
use Doctrine\ORM\EntityManagerInterface;

class ElevatorController extends Controller
{
    /**
     * @Route("/elevator/index", name="elevator_index")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function indexAction(EntityManagerInterface $em, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Elevator::class);
        $nearestElevator = null;
        $formOtherFloor = $this->createFormBuilder()
            ->add('direction', ChoiceType::class, [
               'choices' => [
                   'Up' => 1,
                   'Down' => 2
               ],
               'expanded' => true
            ])
            ->add('departureFloor', NumberType::class, [
                'constraints' => [
                    new Range([
                        'min' => Elevator::GROUND_FLOOR,
                        'max' => Elevator::TOP_FLOOR
                    ])
                ]
            ])
            ->add('send', SubmitType::class)
            ->getForm();

        $formGroundFloor = $this->createFormBuilder()
            ->add('destinationFloor', NumberType::class, [
                'constraints' => [
                    new Range([
                        'min' => Elevator::GROUND_FLOOR,
                        'max' => Elevator::TOP_FLOOR
                    ])
                ]
            ])
            ->add('send', SubmitType::class)
            ->getForm();

        $data = null;
        $departureFloor = null;
        $destinationFloor = null;
        $formOtherFloor->handleRequest($request);
        if ($formOtherFloor->isSubmitted() && $formOtherFloor->isValid()) {
            $data = $formOtherFloor->getData();
            $departureFloor = $data['departureFloor'];
            $destinationFloor = ($data['direction'] == Elevator::UP) ? Elevator::TOP_FLOOR : Elevator::GROUND_FLOOR ;
        }

        $formGroundFloor->handleRequest($request);
        if ($formGroundFloor->isSubmitted() && $formGroundFloor->isValid()) {
            $data = $formGroundFloor->getData();
            $departureFloor = 1;
            $destinationFloor = $data['destinationFloor'];
        }

        if(!is_null($destinationFloor) | !is_null($destinationFloor)) {
            $nearestElevator = $repository->findNearestElevator($destinationFloor, $departureFloor);
            $nearestElevator->setCurrentFloor($destinationFloor);
            $this->saveEntity($nearestElevator, $em);
        }
        $elevators = $repository->findAll();
        return $this->render('elevator/index.html.twig', [
            'elevators' => $elevators,
            'nearestElevator' => $nearestElevator,
            'formOtherFloor' => $formOtherFloor->createView(),
            'formGroundFloor' => $formGroundFloor->createView()
        ]);
    }

    public function saveEntity(Elevator $elevator, EntityManagerInterface $em)
    {
        $em->persist($elevator);
        $em->flush();
    }

    /**
     * @Route("/elevator/create", name="elevator_create")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function createAction(EntityManagerInterface $em)
    {
        for($i = 1; $i <= 4; $i++) {
            $elevator = new Elevator();
            $elevator->setMinFloor(Elevator::GROUND_FLOOR);
            if (($i % 2) === 0) {
                $elevator->setMaxFloor(Elevator::TOP_FLOOR);
            } else {
                $elevator->setMaxFloor(15);
            }
            $elevator->setCurrentFloor(Elevator::GROUND_FLOOR);
            $em->persist($elevator);
        }
        $em->flush();
        $em->clear();
        return $this->render('elevator/create.html.twig');
    }

}