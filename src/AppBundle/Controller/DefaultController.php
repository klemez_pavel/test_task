<?php

namespace AppBundle\Controller;

use Carbon\Carbon;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DefaultController extends Controller
{
    const DAY = 1;
    const MONTH = 9;
    const YEAR = 2017;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/week")
     */

    public function weekAction(Request $request)
    {
        $form = $this->createForm(DateType::class);
        $input = $request->get('date');
        $numberOfWeek = $this->getWeekNumber($input);

        return $this->render('default/week.html.twig', array(
            'form' => $form->createView(),
            'numberOfWeek' => $numberOfWeek,
            'date' => Carbon::CreateFromDate($input['year'], $input['month'], $input['day'])
        ));
    }

    public function getWeekNumber($input)
    {
        $numberOfWeek = null;
        $defaultDate = new DateTime();
        $defaultDate->setDate(self::YEAR, self::MONTH, self::DAY);
        //date adjustment
        $dayNumber = $defaultDate->format("w") - 1;
        //date of monday from start week
        $dateOfMonday = clone $defaultDate;
        $dateOfMonday->modify("- $dayNumber days");
        $date = new Datetime();
        $date->setDate($input['year'], $input['month'], $input['day']);
        if($date < $dateOfMonday) {
            $defaultDate->setDate($input['year']-1, self::MONTH, self::DAY);
            $dayNumber = $defaultDate->format("w") - 1;
            $dateOfMonday = clone $defaultDate;
            $dateOfMonday->modify("- $dayNumber days");
        }
        $diff = $dateOfMonday->diff($date);
        $numberOfWeek = $diff->days / 7 % 4 + 1;
        return $numberOfWeek;
    }
}
