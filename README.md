Symfony Standard Edition
========================
Installing:

1. Clone project
        
        git clone git@bitbucket.org:klemez_pavel/test_task.git
        
        cd test_task
        
2. Create .env file and adapt it to application 
                
        cd docker-symfony
        
        cp .env.dist .env
        
3. Build/run containers with and then 
   update your system host file (add symfony.dev)
    
        docker-compose build
        
        docker-compose up -d

4. Composer install & create database:

        docker-compose exec php bash
       
        composer install
       
5. Prepare Symfony app:
   
    1. Update app/config/parameters.yml
      
            parameters: 
                database_host: db
        
    2. Create database and tables if not exist
    
            sf3 doctrine:database:create
            
            sf3 doctrine:schema:update --force


What's inside?
--------------

1. http://symfony.dev/elevator/create - initialize elevators.
2. http://symfony.dev/elevator/index - elevator dispatcher.
3. http://symfony.dev/week - check for week number.
4. http://symfony.dev:8080 - phpmyadmin.
5. http://symfony.dev:81 Logs (Kibana).

Enjoy!



https://github.com/maxpou/docker-symfony 
